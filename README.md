# AF820 Smart Light

The [AF820 Smart Light](http://www.vstarcam.com/AF820-Smart-Light-141.html) is a WiFi connected LED bulb sold by VStarCam. With its companion smartphone application, the bulb can be controlled from within a local network as well as from the internet. Another nice characteristic of this bulb is that it is very [cheap](http://www.gearbest.com/smart-home/pp_211823.html) ($16 or less) when compared with competitors such as the [Philips Hue](https://www.amazon.com/Philips-456202-White-Ambiance-Extension/dp/B017OR0IVM/ref=sr_1_5?s=hi&ie=UTF8&qid=1469391631&sr=1-5&keywords=philips+hue) which requires an additional bridging device to connect to a home WiFi network. Granted, the Hue is a more polished solution but you can likely buy 3-4 AF820 Smart Lights for the cost of a single Hue bulb. Plus, since the AF820 connects directly to a local access point (AP) there is no need for a bridge device.

Until now, one problem with AF820 Smart Light was that it could only be controlled with VStarCam's proprietary client [application](https://play.google.com/store/apps/details?id=vstc.vscam.client). This made it difficult to integrate with existing home automation solutions like [OpenHAB](http://www.openhab.org/) or Amazon's [Echo](https://www.amazon.com/Amazon-Echo-Bluetooth-Speaker-with-WiFi-Alexa/dp/B00X4WHP5E). This is about to change. This project provides a Node.js based MQTT application that can be used to control one (or more) AF820 bulbs in a home network. It also attempts to describe the protocol used to communicate with the AF820 in some detail should others wish to talk to the bulb using another method or language besides MQTT or Node.js.

## Protocol

The protocol the VStarCam application uses to communicate with the AF820 is carried by UDP messages delivered to the bulb. Communication occurs on port 5880 and the minimum (basic) size packet is 37 bytes long. This size may be extended based on the command being sent to the bulb. It appears all numbers are encoded in network (big endian) order. Each device (e.g. phone) controlling the Smart Light identifies itself with a four byte unique identifier. It's not clear if the bulb interprets this field but it's included with every control packet sent to the bulb.

Since UDP is an unreliable protocol, not every request to the bulb is acknowledged due to either the request failing to be delivered to the bulb or the bulb's response being lost. As a result, it is often necessary to make several attempts to configure the bulb. Fortunately, for the most common commands the acknowledgment/response from the bulb contains its full state. This is helpful in determining if the expected state matches the requested state. See the contents of the [Check Status Response](#check-status-response) below.

The contents of a command/control packet sent to the bulb over UDP broadcast (or directly to it's IP address if known) is shown below:



| Packet Field | Description |
|:-------------|:------------|
| HDR          | Packet header. Either 0x55 0xAA (for a local message) or possibly 0x55 0x66 for a message sent to a remote device.|
| PLEN         | The total packet length (in bytes) including any extra data beyond the *minimum* sized packet.|
| FIX          | A fixed constant sequence of bytes that does not appear to change.|
| DEVICE MAC   | The MAC of the bulb being controlled.|
| DC           | Device code.|
| DV           | Device version.|
| DF           | Device function, e.g. the command to execute.|
| TB           | Type big (0 or 1). Might refer to the size of the bulb but its true meaning remains unknown.|
| TS           | Type small (0 or 1). Again, might refer to the size of the bulb but its meaning remains unknown.|
| CTRLID       | A unique controller identifier for the device (phone) communicating with the bulb. It's not clear whether the bulb interprets this identifier in any way.|
| SEQ          | A monotonically increasing sequence number for each packet. It is never zero.|
| XDL          | The length (in bytes) of the extra data beyond the minimum packet size (37 bytes).|
| XDAT         | The *extra* data associated with the packet.|

The AF820 supports the following commands:

**Note:** All values are in hexadecimal

---

### Turn On

#### Description
This command turns **on** the AF820.

#### Response
Responds with a [Check Status Response](#check-status-response).

#### Raw Packet

| HDR | PLEN | FIX  | DEVICE MAC     |FIX   |DC |DV |DF |TB |TS |CTRLID  |FIX     |SEQ |XDL |FIX |XDAT|
|:----|:-----|:-----|:---------------|:-----|:--|:--|:--|:--|:--|:-------|:-------|:---|:---|:---|:---|
|55aa |0025  |010000|000000005edab000|000000|20 |00 |01 |00 |00 |c43e3b51|00000000|18d4|0000|0000|-   |

#### MQTT Command
```
turnOn
```
---

### Turn Off

#### Description
This command turns **off** the AF820.

#### Response
Responds with a [Check Status Response](#check-status-response).

#### Raw Packet

| HDR | PLEN | FIX  | DEVICE MAC     |FIX   |DC |DV |DF |TB |TS |CTRLID  |FIX     |SEQ |XDL |FIX |XDAT|
|:----|:-----|:-----|:---------------|:-----|:--|:--|:--|:--|:--|:-------|:-------|:---|:---|:---|:---|
|55aa |0025  |010000|000000005edab000|000000|20 |00 |02 |00 |00 |c43e3b51|00000000|18d4|0000|0000|-   |

#### MQTT Command
```
turnOff
```
---

### Set Random

#### Description
Enables or disables cycling through random colors.

#### Response
Responds with a [Check Status Response](#check-status-response).

#### Raw Packet

| HDR | PLEN | FIX  | DEVICE MAC     |FIX   |DC |DV |DF |TB |TS |CTRLID  |FIX     |SEQ |XDL |FIX |XDAT|
|:----|:-----|:-----|:---------------|:-----|:--|:--|:--|:--|:--|:-------|:-------|:---|:---|:---|:---|
|55aa |0026  |010000|000000005edab000|000000|20 |00 |04 |00 |00 |c43e3b51|00000000|18d4|0001|0000|00  |

#### Extra Data

|XDAT|Byte Offset|Description|
|:--------|:----------|:------------------|
|Enable|0| Enable cycling the color (1) or disable (0).|

#### MQTT Command
```
setRandom <true/false>
```
---

### Set Color

#### Description
Sets the color of the bulb.

#### Response
Responds with a [Check Status Response](#check-status-response).

#### Raw Packet

| HDR | PLEN | FIX  | DEVICE MAC     |FIX   |DC |DV |DF |TB |TS |CTRLID  |FIX     |SEQ |XDL |FIX |XDAT|
|:----|:-----|:-----|:---------------|:-----|:--|:--|:--|:--|:--|:-------|:-------|:---|:---|:---|:---|
|55aa |0031  |010000|000000005edab000|000000|20 |00 |03 |00 |00 |c43e3b51|00000000|18d4|000C|0000|RRRRGGGGBBBBWWWWTTTTLLLL|

#### Extra Data

|XDAT|Byte Offset|Description|
|:--------|:----------|:------------------|
|Red|0| Two byte red value in range 0x00-0xFF.|
|Green|2| Two byte green value in range 0x00-0xFF.|
|Blue|4| Two byte blue value in range 0x00-0xFF.|
|White|6|Two byte white value in range 0x00-0xFF.|
|Time|8|The time (msec) to transition to the desired color. Maximum value = 2000.|
|Lux|10|LUX value for color model. Unclear how this impacts color.|

#### MQTT Command
```
setColor <red> <green> <blue> <white> <time> <lux>
```
---

### Sleep

#### Description
Sets the time (in seconds) before the bulb turns **off**.

#### Response
Responds with a [Check Status Response](#check-status-response).

#### Raw Packet

| HDR | PLEN | FIX  | DEVICE MAC     |FIX   |DC |DV |DF |TB |TS |CTRLID  |FIX     |SEQ |XDL |FIX |XDAT|
|:----|:-----|:-----|:---------------|:-----|:--|:--|:--|:--|:--|:-------|:-------|:---|:---|:---|:---|
|55aa |0027  |010000|000000005edab000|000000|20 |00 |05 |00 |00 |c43e3b51|00000000|18d4|0002|0000|TTTT|

#### Extra Data

|XDAT     |Byte Offset|Description        |
|:--------|:-----     |:------------------|
|Time     |0          | The time (in seconds) before turning off the light.|

#### MQTT Command
```
sleep <time>
```
---

### Check Status

#### Description
Retrieves the general status of the bulb.

#### Response
Responds with a [Check Status Response](#check-status-response).

#### Raw Packet

| HDR | PLEN | FIX  | DEVICE MAC     |FIX   |DC |DV |DF |TB |TS |CTRLID  |FIX     |SEQ |XDL |FIX |XDAT|
|:----|:-----|:-----|:---------------|:-----|:--|:--|:--|:--|:--|:-------|:-------|:---|:---|:---|:---|
|55aa |0025  |010000|000000005edab000|000000|20 |00 |ff |00 |00 |c43e3b51|00000000|18d4|0000|0000|-   |

#### MQTT Command
```
checkStatus
```
---

### Check WiFi

#### Description
Retrieves the SSID and password of the access point (AP) the bulb is connected to.

#### Response
Responds with the SSID and associated password of the configured access point. This information is sent in the clear and it **not** encrypted in any way.

#### Raw Packet

| HDR | PLEN | FIX  | DEVICE MAC     |FIX   |DC |DV |DF |TB |TS |CTRLID  |FIX     |SEQ |XDL |FIX |XDAT|
|:----|:-----|:-----|:---------------|:-----|:--|:--|:--|:--|:--|:-------|:-------|:---|:---|:---|:---|
|55aa |0026  |010000|000000005edab000|000000|20 |00 |f0 |00 |00 |c43e3b51|00000000|18d4|0001|0000|01  |

#### MQTT Command
```
checkWifi
```
---

### Set WiFi

#### Description
Sets the SSID and password of the access point (AP) the smart light should connect to. This command does **not** cause the light to connect to the AP.

#### Response
Returns a [Set WiFi Response](#set-wifi-response) packet.

#### Raw Packet

| HDR | PLEN | FIX  | DEVICE MAC     |FIX   |DC |DV |DF |TB |TS |CTRLID  |FIX     |SEQ |XDL |FIX |XDAT|
|:----|:-----|:-----|:---------------|:-----|:--|:--|:--|:--|:--|:-------|:-------|:---|:---|:---|:---|
|55aa |00??  |010000|000000005edab000|000000|20 |00 |f0 |00 |00 |c43e3b51|00000000|18d4|0001|0000|03#SSID#PWD|

PLEN = 37 + 3 + SSID length + password length


|XDAT|Byte Offset|Description|
|:--------|:----------|:------------------|
| Sub-command|0|0x03 => Set WiFi access credentials.|
|SSID Length|1|The length of the SSID (not NULL terminated).|
|SSID|2| The SSID string.|
|Password Length|2 + Length SSID|The length of the password (not NULL terminated).|
|Password|3 + Length of SSID|The password for the WiFi access point.|

#### MQTT Command
```
setWifi <ssid>  <password>
```
---

### Restart WiFi

#### Description
Re-starts the WiFi on the smart light. It will re-connect with the access point (AP) using the credentials specified by the *setWifi* command.

#### Response
No response from the smart light.

#### Raw Packet

| HDR | PLEN | FIX  | DEVICE MAC     |FIX   |DC |DV |DF |TB |TS |CTRLID  |FIX     |SEQ |XDL |FIX |XDAT|
|:----|:-----|:-----|:---------------|:-----|:--|:--|:--|:--|:--|:-------|:-------|:---|:---|:---|:---|
|55aa |0026  |010000|000000005edab000|000000|20 |00 |f0 |00 |00 |c43e3b51|00000000|18d4|0001|0000|05|

#### MQTT Command
```
restartWifi
```
---

### Reset

#### Description
Resets the smart light to its default (unpaired) settings. After this call the smart light will need to be re-programmed to join a specific access point.

#### Response

No response from the smart light.

#### Raw Packet

| HDR | PLEN | FIX  | DEVICE MAC     |FIX   |DC |DV |DF |TB |TS |CTRLID  |FIX     |SEQ |XDL |FIX |XDAT|
|:----|:-----|:-----|:---------------|:-----|:--|:--|:--|:--|:--|:-------|:-------|:---|:---|:---|:---|
|55aa |0025  |010000|000000005edab000|000000|20 |00 |f7 |00 |00 |c43e3b51|00000000|18d4|0000|0000|-   |

#### MQTT Command
```
reset
```
---

### Register

#### Description

This command appears to register the device with a remote registration service running at the following IP address: 198.199.115.33:30001

#### Response

A response packet containing the IP address and port of the registration service.

#### Raw Packet
| HDR | PLEN | FIX  | DEVICE MAC     |FIX   |DC |DV |DF |TB |TS |CTRLID  |FIX     |SEQ |XDL |FIX |XDAT|
|:----|:-----|:-----|:---------------|:-----|:--|:--|:--|:--|:--|:-------|:-------|:---|:---|:---|:---|
|5566 |0025  |010000|000000005edab000|000000|20 |00 |fb |00 |00 |c43e3b51|00000000|18d4|0000|0000|-   |

#### MQTT Command
```
register
```
---

### Check Status Response

#### Description
This is a common response message returned as an acknowledgement to multiple command requests.

#### Raw Packet
| HDR | PLEN | FIX  | DEVICE MAC     |FIX   |DC |DV |DF |TB |TS |CTRLID  |FIX     |SEQ |XDL |FIX |XDAT|
|:----|:-----|:-----|:---------------|:-----|:--|:--|:--|:--|:--|:-------|:-------|:---|:---|:---|:---|
|55aa |0035  |010000|000000005edab000|000000|20 |00 |fe |00 |00 |c43e3b51|00000000|18d4|0010|0000|00000000000000000000000000000000|

#### Extra Data

|XDAT     |Byte Offset|Description|
|:--------|:----------|:----------|
|Status   |0          |Indicates whether light is on (1) or off (0).|
|Random   |1          |Indicates whether light is flashing random colors (1) or not (0).|
|Red      |2          | Two byte red value in range 0x00-0xFF.|
|Green    |4          | Two byte green value in range 0x00-0xFF.|
|Blue     |6          | Two byte blue value in range 0x00-0xFF.|
|White    |8          |Two byte white value in range 0x00-0xFF.|
|Time     |10         |The time of day?|
|Lux      |12         |LUX value for color model.|
|Sleep    |14         |The time before turning off the light (in seconds).|

---

### Register Response

#### Description
A response packet containing the IP address and port of the registration service. This is the response to a registration request.

#### Raw Packet

| HDR | PLEN | FIX  | DEVICE MAC     |FIX   |DC |DV |DF |TB |TS |CTRLID  |FIX     |SEQ |XDL |FIX |XDAT|
|:----|:-----|:-----|:---------------|:-----|:--|:--|:--|:--|:--|:-------|:-------|:---|:---|:---|:---|
|5566 |002b  |010000|000000005edab000|000000|20 |00 |fa |00 |00 |c43e3b51|00000000|18d4|0006|0000|0000000000|

#### Extra Data

|XDAT       |Byte Offset|Description|
|:----------|:----------|:----------|
|IP Address |0          |The IP address of the registration service.|
|Port       |4          |The port associated with the IP address.|

---

### Set WiFi Response

#### Description
The response to the request to set the WiFi access point used by the smart light.

#### Response Packet

| HDR | PLEN | FIX  | DEVICE MAC     |FIX   |DC |DV |DF |TB |TS |CTRLID  |FIX     |SEQ |XDL |FIX |XDAT|
|:----|:-----|:-----|:---------------|:-----|:--|:--|:--|:--|:--|:-------|:-------|:---|:---|:---|:---|
|55aa |0026  |010000|000000005edab000|000000|20 |00 |f0 |00 |00 |c43e3b51|00000000|18d4|0001|0000|04  |

#### Extra Data

|XDAT        |Byte Offset|Description|
|:-----------|:----------|:----------|
|Sub-command |0          |0x04 => Acknowledgement of WiFi AP credentials.|

---

## Installation Prerequisites

The AF820 Smart Light service is written in Node.js. Although developed with Node.js v6.1.0 it likely supports earlier versions. The safe choice, however, is version 6.1.0 or newer if possible.

The Smart Light service also expects to be able to connect to a MQTT broker but this broker does **not** need to be on the same (local) network as the Smart Light service. Since the Smart Light service will typically be communicating with the LED bulb(s) over broadcast UDP it's expected to run on the same local WiFi network (and access point) as the bulb(s).

It is expected that the MAC addresses of the Smart Light(s) that will be controlled are available. These can be found by running the VStarCam application and looking under *Device Management*. Here the MAC will appear as the identifier for the linked bulb. It's recommended the initial set up and configuration of the Smart Light be done with the manufacturer's [application](https://play.google.com/store/apps/details?id=vstc.vscam.client).

## Installation

To install the project simply clone the development repository, edit the application and log4js configuration files to suit your environment, install the Node.js dependencies, and run the program.

```
# git clone https://gitlab.com/iot/af820smartlight.git
# cd af820smartlight
# npm install
<-- Edit the configuration files in cfg -->
# node smartlight.js -c cfg/smartlight.json -l cfg/log4js.json --mqtt
```

### Application Configuration

The application configuration file is fairly straightforward. Below is an example:

```javascript
{
  "mqtt"  : {
    "url" : "mqtt://192.168.0.112:1883",
    "user" : "",
    "password" : "",
    "topicWill" : "smartlight/online"
  },
  "bulbs" : {
	  "office" : {
		  "device": "000000005cdaf000",
		  "cmdTopic" : "smartlight/office/cmd",
		  "statusTopic" : "smartlight/office/status"
	  },
	  "bedroom": {
  	  "device": "000000005cdaf000",
  	  "cmdTopic" : "smartlight/bedroom/cmd",
  	  "statusTopic" : "smartlight/bedroom/status"
    }
  }
}
```

Currently the primary application configuration consists of the MQTT connection settings and topics.

| Key         | Description                                                                                    |
|:------------|:-----------------------------------------------------------------------------------------------|
| url         | Any valid MQTT URL accepted by the Node.js [MQTT](https://www.npmjs.com/package/mqtt) package. |
| user        | The user name associated with the MQTT broker.                                                 |
| password    | The password required by the MQTT broker.                                                      |
| topicWill   | The MQTT last will (LWT) registered with the broker by the Smart Light service.                |

#### Bulbs

The bulbs that the application can control are listed in this section and are given a name (e.g. "office", "bedroom", etc...). Each name **must** be unique and include the following properties:

| Key             | Description                                                                                                    |
|:---------------|:-----------------------------------------------------------------------------------------------|
| device         | The bulb's Smart Light 8-byte MAC address expressed as a hex string.    |
| cmdTopic    | A unique MQTT topic where the bulb will subscribe for commands.    |
| statusTopic | A unique MQTT topic where the bulb will publish the results of a comand. |

### Log Configuration

The logging configuration follows the conventions defined by the [log4js](https://www.npmjs.com/package/log4js) package. See this module's documentation to understand the logging options that are available.

### Operation

The application offers two modes of operation: command-line and MQTT client.

#### Command-line Mode

The command-line operating mode allows a single requests to be issued to a bulb and prints out the results. On the command-line, either the bulb's device MAC address should be supplied, or if it is supplied a configuration file, the bulb's name. For instance, to turn on the bulb issue:

```
node ./bin/smartlight.js -d "000000005cdaf000" turnOn
```

or

```
node ./bin/smartlight.js -b office turnOn
```

Other options are available as well as shown below:

```
node ./bin/smartlight.js --help

Usage:

In MQTT client (daemon) mode:
bin/smartlight.js -c|--cfg=<path_to_cfg> -m|--mqtt [-l|--log4js=<path_to_cfg>]

In command-line mode:
bin/smartlight.js [-c|--cfg=<path_to_cfg>] [-l|--log4js=<path_to_cfg>]
[-d|--device=<device_ID>] [-b|--bulb=<bulb_name>] cmd [args..]

Commands:
  checkWifi      Check the status of the WiFi.

  checkStatus    Check the status of the bulb.

  ramp           Ramp up/down the light over a period of minutes to simulate
                 sunrise/sunset.
                 Args: <up|down> <time>

  restartWifi    Restarts WiFi and re-attaches to the configured AP.

  reset          Resets the smart light to its default (unpaired) settings.

  register       Register the bulb with a remote registration service.

  setRandom      Enables or disables cycling through random colors.
                 Args: <true|false>

  setColor       Sets the color of the bulb. Colors and lux range [0-255]. Time
                 is in range [0-2000] msec.
                 Args: <red> <green> <blue> <white> <time> <lux>

  sleep          Sets the time (in seconds) before the bulb turns off.
                 Args: <sleepTime>

  setWifi        Sets the SSID and password for the AP.
                 Args: <ssid> <password>

  setBrightness  Change the (rel)ative or (abs)olute brightness of the bulb over
                 time.
                 Args: <rel|abs> <percent> <time>

  turnOn         Turn on the bulb.

  turnOff        Turn off the bulb.


Options:
  -c, --cfg      Path to the AF820 Smart Light configuration file.
                                                [default: "cfg/smartlight.json"]
  -l, --log4js   Path to the LOG4JS configuration file.
                                                    [default: "cfg/log4js.json"]
  -m, --mqtt     Run as a daemon and processes commands and provide responses to
                 an MQTT broker.                                       [boolean]
  -d, --device   Eight byte device identifier specified as a hex string.[string]
  -b, --bulb     Bulb name as identified in configuration file.         [string]
  -v, --verbose  Enables verbose debug output.                           [count]
  -h, --help     Show help                                             [boolean]
```

#### MQTT Mode

In MQTT mode the application runs continuously as an MQTT client and receives and responses to command over the network. This mode is invoked by specifying the '-m|--mqtt' option on the command-line. The configuration file is parsed to know how to contact the MQTT broker and which topics to subscribe and publish based on the bulbs that are defined. At a minimum, to run the application in MQTT mode the following parameters must be specified:

```
node ./bin/smartlight.js -c ./cfg/smartlight.json -m
```

or equivalently using NPM

```
npm start -- -c ./cfg/smartlght.json -m
```

You can exit the program by hitting CTRL-C at the keyboard.

Commands can be issued to the configured MQTT command topics as simple text string. These commands are identical to the ones issued at the command-line and include arguments separated by spaces. The response to a request is published to the *status* topic associated with the bulb. The response is a comma separated text string containing the following parameters.

** On Error **
```
<cmd>,ERROR,[optional message]
```

Example:

```
checkStatus,ERROR,Error: Timeout
```

** On Success **
```
<cmd>,OK,arg1=value1,arg2=value2,...
```

Example:

```
checkStatus,OK,devMac=0x000000005edac000,ctrlDevId=0x23b207f0,devFuncId=254,devVer=0,devCode=32,lightOn=true,random=false,red=0,green=0,blue=0,white=63,time=10,lux=255,sleepTime=0
```
